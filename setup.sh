#!/bin/bash

set -e

BASE_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

ask() {
    question=$1

    read -r -p "$question [y/N] " response
    case "$response" in
        y)
            echo true
            ;;
        *)
            echo false
            ;;
    esac

}

repoExists() {
    repoUrl=$1
    echo `zypper lr -u | grep -F "$repoUrl" | wc -l`
}

addRepo() {
    location=$1
    repoUrl=$location
    alias=$2
    askToInstall=$3
    continueInstallation=1
    printSkip=$4 || true

    if [[ $askToInstall == "ask" ]]; then
        continueInstallation=`ask "Install $alias ($location) repo?"`
    fi

    # Check if this is a real file or a url
    if [[ -f "$location" ]]; then
        repoUrl=`cat "$location" | grep -i baseurl | cut -f 2 -d "="`
    fi

    if [[ $continueInstallation -eq 1 && `repoExists $repoUrl` -eq 0 ]]; then
        (set -x; zypper ar -f -r "$location" "$alias" >> /tmp/setup-repos.log)
        echo ""
    elif [[ $continueInstallation -eq 1 ]]; then
        if [[ $printSkip ]]; then
            echo "Repository $repoUrl already exists! Skipping..."
        fi
    fi
}

mainMenu() {
    echo `dialog --stdout --title "Main menu" --menu "Please choose an option" 15 50 10 \
1 "Configure repositories" \
2 "Install Apps" \
3 "Install Nvidia drivers" \
4 "Install Oracle JDK"`
}

configReposMenu() {
    REPOS_PATH=$BASE_DIR/repos
    if [[ `ls $REPOS_PATH/*.repo | wc -l` -eq 0 ]]; then
        response=$(dialog --stdout --title "Configure repositories" --yesno "We couldn't find any repositories in ./repos/! Would you like to go back to main menu?" 8 50)
        if [ $? -eq 0 ]; then
            bootstrap
        fi
    else
        repos=()
        i=1

        for f in $REPOS_PATH/*.repo; do
            url=`cat $f | grep -i baseurl | cut -f 2 -d "="`
            name=`cat $f | grep -i name | cut -f 2 -d "="`
            selected=off
            exists=`repoExists $url`
            existsText=""
            if [[ $exists -eq 1 ]]; then
                selected=on
                existsText=" (exists)"
            fi

            repos+=("`basename $f`" "$existsText" "$selected")
            i=$[i+1]
        done

        choices=$(dialog --stdout --checklist 'Choose repositories to add:' 20 60 10 "${repos[@]}")

        if [[ $? -eq 0 ]]
        then
            i=1
            progress=()
            choiceCount=`echo $choices | grep -o "repo" | wc -l`
            for repo in $choices
            do
                repoUrl=`cat "$REPOS_PATH/$repo" | grep -i baseurl | cut -f 2 -d "="`

                addRepo "$REPOS_PATH/$repo"

                percentage=`echo "scale=2; $i / $choiceCount * 100" | bc `
                exists=`repoExists $repoUrl`
                if [[ $exists -eq 1 ]]; then
                    progress+=("$repo" "0")
                else
                    progress+=("$repo" "1")
                fi

                dialog --title "Adding repositories.." --mixedgauge "If errors occurs check /tmp/setup-repos.log." 0 0 ${percentage%.*} "${progress[@]}"

                i=$[i+1]
                sleep 1
            done

            sleep 2

            dialog --title "Refresh repositories?" --yesno "Should we refresh the repositories now?" 7 60
            REFRESH_REPOS=$?

            if [[ $REFRESH_REPOS -eq 0 ]]; then
                zypper refresh
            fi

            bootstrap
        else
            bootstrap
        fi
    fi
}

installAppsMenu() {
    APPS_PATH=$BASE_DIR/apps
    if [[ `ls $APPS_PATH/*.sh | wc -l` -eq 0 ]]; then
        response=$(dialog --stdout --title "Choose apps to install" --yesno "We couldn't find any repositories in ./apps/! Would you like to go back to main menu?" 8 50)
        if [ $? -eq 0 ]; then
            bootstrap
        fi
    else
        apps=()
        i=1

        for f in $APPS_PATH/*.sh; do
            if [[ -x "$f" ]]; then
                selected=off
                apps+=("`basename $f`" "" "$selected")
                i=$[i+1]
            fi
        done

        if [[ "x${apps[@]}" -eq "x" ]]; then
            echo "No executable files found in $APPS_PATH"
            exit
        fi

        choices=$(dialog --stdout --checklist 'Choose app to install:' 20 60 10 "${apps[@]}")

        if [[ $? -eq 0 ]]
        then
            echo "" > /tmp/setup-apps.log
            i=1
            progress=()
            choiceCount=`echo $choices | grep -o ".sh" | wc -l`
            for app in $choices
            do
                percentage=`echo "scale=2; $i / $choiceCount * 100" | bc `

                tempProgress=("${progress[@]}")
                tempProgress+=("$app" "7")

                dialog --title "Installing apps.." --mixedgauge "If errors occurs check /tmp/setup-apps.log." 0 0 ${percentage%.*} "${tempProgress[@]}"

                $APPS_PATH/$app | tee -a /tmp/setup-apps.log

                progress+=("$app" "3")

                i=$[i+1]
                sleep 1
            done

            sleep 2

            bootstrap
        else
            bootstrap
        fi

    fi


}

installNvidiaDriver() {
    INSTALL_NVIDIA=387.22
    if `ask "Installing nvidia requires some dependencies. Install them now?"`; then
        echo "Installing kernel dependencies..."
        kernelPackages=`zypper search -si kernel | cut -f 2 -d "|" | tail -n +6 | uniq | grep -v "patterns" | grep -v devel_kernel | xargs`
        zypper in --force --repo kernel:stable $kernelPackages
        echo ""

        echo "Installing zypper patterns to be able to build nVidia drivers..."
        zypper in -t pattern "devel_basis" "devel_C_C++" "devel_kernel" "devel_rpm_build"
        echo ""
        echo "New kernel has been installed. Please reboot and run this script again, skipping this step!"
        echo ""
        exit 0
    fi

    NVIDIA_RUN="/usr/src/NVIDIA-Linux-x86_64-$INSTALL_NVIDIA.run"

    if [ ! -e $NVIDIA_RUN ]; then
        echo "Downloading driver version $INSTALL_NVIDIA into $NVIDIA_RUN"
                                 #https://uk.download.nvidia.com/XFree86/Linux-x86_64/384.90/NVIDIA-Linux-x86_64-384.90.run
        (set -x; curl -j -L -o $NVIDIA_RUN https://uk.download.nvidia.com/XFree86/Linux-x86_64/$INSTALL_NVIDIA/NVIDIA-Linux-x86_64-$INSTALL_NVIDIA.run)
        chmod 750 $NVIDIA_RUN
        echo ""
    else
        echo "Driver has alredy been downloaded... skipping download."
    fi

    NOUVEAU=0
    echo -n "Is nouveau module is currently being used? "
    if [ `lsmod | grep -i nouveau | wc -l` -gt 0 ]; then
        echo "yes - nvidia installation will fail!"
        echo ""
        echo "To fix nouveu module needs to be blacklisted in modprobe.d."
        if `ask "Should we blacklist the nouveu module?"`; then
            echo "blacklist nouveau" > /etc/modprobe.d/nvidia.conf
            mkinitrd
            echo ""
            echo "Reboot and run this script again."
            exit 0
        else
            echo "Sorry, nothing more to do here. Exiting."
            exit 1
        fi
    else
        echo "no - we can proceed"
    fi

    if [ `service --status-all | grep -i display-manager.service | grep -i running | wc -l` -gt 0 ]; then
        echo ""
        echo "Warning: You are currently running rcxdm!"
        echo "Installing the driver will require you to stop Xorg/Wayland."
        echo "You can do this by doing CTRL + ALT + F4; enter a valid user/pass; write rcxdm stop; run this script again!"
        echo ""
        exit 1
    fi

    echo "Installing nVidia driver $INSTALL_NVIDIA"
    $NVIDIA_RUN
    echo ""
    echo "If the installation was successful - reboot and enjoy."
}



bootstrap() {

    mainMenuChoice=$(mainMenu)

    if [[ $mainMenuChoice -eq 1 ]]; then
        configReposMenu
    elif [[ $mainMenuChoice -eq 2 ]]; then
        installAppsMenu
    elif [[ $mainMenuChoice -eq 3 ]]; then
        #installNvidiaMenu
        installNvidiaDriver
    elif [[ $mainMenuChoice -eq 4 ]]; then
        installOracleJDKMenu
    fi
}

if [[ `dialog --version 2>/dev/null || echo "no-dialog"` == "no-dialog" ]]; then
    if `ask "This script requires 'dialog' to work. Install it now?"`; then
        (set -x; sudo zypper in -y dialog)
    else
        echo "Nothing more to do here."
    fi
fi
if [[ `bc --version 2>/dev/null || echo "no-bc"` == "no-bc" ]]; then
    if `ask "This script requires 'bc' to work. Install it now?"`; then
        (set -x; sudo zypper in -y bc)
    else
        echo "Nothing more to do here."
    fi
fi

bootstrap






#if [ $? -eq 0 ]
#then
#        for choice in $choices
#        do
#                echo "You chose: $choice"
#        done
#else
#        echo cancel selected
#fi


exit

usage() {
    cat <<EOF

Usage: $0 [options]

Options:
    --install-nvidia-driver [VERSION]  Installs nVidia drivers with the specified version. (Requires proper repositories from --install-repos!)
    --install-repos                    Adds necessary repositories to zypper (kernel, 1dot75cm, nodejs, php, etc)
    --install-pulseaudio               Installs PulseAudio packages.
    --install-apps
    --install-jdk                      Installs Oracle jdk into /java/jdk/
    --install-wps                      Installs WPS + fetchmsttfonts
    --install-video-codecs             Installs video codecs using packman repository
    --help                             Show this message

EOF

}

ask() {
    question=$1

    read -r -p "$question [y/N] " response
    case "$response" in
        y)
            echo true
            ;;
        *)
            echo false
            ;;
    esac

}

repoExists() {
    repoUrl=$1
    echo `zypper lr -u | grep -F "$repoUrl" | wc -l`
}

addRepo() {
    url=$1
    alias=$2
    askToInstall=$3
    continueInstallation=1
    printSkip=$4 || true

    if [[ $askToInstall == "ask" ]]; then
        continueInstallation=`ask "Install $alias ($url) repo?"`
    fi

    if [[ $continueInstallation -eq 1 && `repoExists $url` -eq 0 ]]; then
        (set -x; zypper ar -f "$url" "$alias")
        echo ""
    elif [[ $continueInstallation -eq 1 ]]; then
        if [[ $printSkip ]]; then
            echo "Repository $url already exists! Skipping..."
        fi
    fi
}

installRepos() {
    echo "Adding zypper repositories..."

    addRepo "https://download.opensuse.org/repositories/Kernel:/stable/standard" "kernel:stable"
    addRepo "https://download.opensuse.org/repositories/home:/1dot75cm:/branches:/X11:/Deepin/openSUSE_Tumbleweed" "deepin" "ask"
    addRepo "https://download.opensuse.org/repositories/devel:/languages:/nodejs/openSUSE_Tumbleweed" "nodejs8" "ask"
    addRepo "https://download.opensuse.org/repositories/devel:/languages:/php/openSUSE_Factory/" "php7" "ask"
    addRepo "https://repo.skype.com/rpm/stable/" "skype" "ask"

    if `ask "Refresh repositories now?"`; then
        zypper refresh
    fi
}


installApps() {
    echo "Installing basic apps..."
    echo " - Installing Google Chrome (if installation states boken packages - ignore)"
    zypper in -y --no-force-resolution https://dl.google.com/linux/direct/google-chrome-stable_current_x86_64.rpm
    echo ""

    echo " - Installing Skype (if installation states boken packages - ignore)"
    zypper in -y --no-force-resolution skypeforlinux
}

installGoogleChrome() {
    echo "Installing Google Chrome"
    echo " - Adding google signing key..."
    rpm --import https://dl.google.com/linux/linux_signing_key.pub
}

installPulseAudio() {
    echo "Installing PulseAudio packages..."
    zypper in `zypper search pulseaudio | grep -v xfce | grep -v gdm | tail -n +6 | cut -f 2 -d "|"  | xargs`
}

installOracleJDK() {
    # Params
    JDK_URL_VERSION="8u131"
    JDK_URL_VERSION_PREFIX="-b11"
    JDK_PATH_VERSION="1.8.0_131"
    JDK_HASH=d54c1d3a095b4ff2b6607d096fa80163
    JDK_URL="${JDK_URL_VERSION}${JDK_URL_VERSION_PREFIX}/${JDK_HASH}/jdk-${JDK_URL_VERSION}-linux-x64"
    MVN_VERSION=3.3.9

    # Beginning installation
    mkdir -p /java
    (set -x; curl -j -L -C - -b "oraclelicense=accept-securebackup-cookie" "http://download.oracle.com/otn-pub/java/jdk/${JDK_URL}.tar.gz" | tar -xz -C /java)
    ln -s /java/jdk${JDK_PATH_VERSION} /java/jdk
    JAVA_HOME=/java/jdk

    # Download maven and install it.
    (set -x; curl -L -C - "http://apache.cbox.biz/maven/maven-3/${MVN_VERSION}/binaries/apache-maven-${MVN_VERSION}-bin.tar.gz" | tar -xz -C /java)
    M2_HOME=/java/apache-maven-${MVN_VERSION}

    # update-alternatives
    echo "Updating update-alternatives..."
    /usr/sbin/update-alternatives --install    "/usr/bin/java" "java" "${JAVA_HOME}/bin/java" 99999
    /usr/sbin/update-alternatives --set java   "${JAVA_HOME}/bin/java"
    /usr/sbin/update-alternatives --install    "/usr/bin/javac" "javac" "${JAVA_HOME}/bin/javac" 99999
    /usr/sbin/update-alternatives --set javac  "${JAVA_HOME}/bin/javac"
    /usr/sbin/update-alternatives --install    "/usr/bin/mvn" "mvn" "${M2_HOME}/bin/mvn" 99999
    /usr/sbin/update-alternatives --set mvn    "${M2_HOME}/bin/mvn"
}


installWPS() {
    echo "Installing WPS Office Suite..."
    zypper in -y http://kdl1.cache.wps.com/ksodl/download/linux/a21//wps-office-10.1.0.5707-1.a21.x86_64.rpm

    echo "Installing WPS fonts..."
    zypper in -y http://kdl.cc.ksosoft.com/wps-community/download/fonts/wps-office-fonts-1.0-1.noarch.rpm

    echo "Installing Microsoft fonts..."
    zypper in -y fetchmsttfonts
}

installVideoCodecs() {
    echo "Checking repositories... (adding packman and dvd if necessary)"
    addRepo "http://packman.inode.at/suse/openSUSE_Tumbleweed/" "pacman"
    addRepo "http://opensuse-guide.org/repo/openSUSE_Tumbleweed/" "dvd"

    # Checkout http://opensuse-guide.org/codecs.php
    echo "Installing necessary packages."
    zypper in -y ffmpeg lame gstreamer-plugins-bad gstreamer-plugins-ugly gstreamer-plugins-ugly-orig-addon gstreamer-plugins-libav libdvdcss2
}

# Process arguments.
SHORT=h,
LONG=help,install-nvidia-driver:,install-repos,install-pulseaudio,install-apps,install-jdk,install-wps,install-video-codecs
# -temporarily store output to be able to check for errors
# -activate advanced mode getopt quoting e.g. via “--options”
# -pass arguments only via   -- "$@"   to separate them correctly
PARSED=`getopt -n 'parse-options' --options $SHORT --longoptions $LONG --name "$0" -- "$@"`
if [[ $? -ne 0 ]]; then
    # e.g. $? == 1
    #  then getopt has complained about wrong arguments to stdout
    exit 2
fi

if [[ $# -eq 0 ]]; then
    usage
    exit 3
fi


# use eval with "$PARSED" to properly handle the quoting
eval set -- "$PARSED"

while true; do
    case "$1" in
        --install-nvidia-driver)
            INSTALL_NVIDIA="$2"
            shift 2
        ;;
        --install-repos)
            INSTALL_REPOS=1
            shift
        ;;
        --install-pulseaudio)
            INSTALL_PULSEAUDIO=1
            shift
        ;;
        --install-apps)
            INSTALL_APPS=1
            shift
        ;;
        --install-jdk)
            INSTALL_JDK=1
            shift
        ;;
        --install-wps)
            INSTALL_WPS=1
            shift
        ;;
        --install-video-codecs)
            INSTALL_VIDEO_CODECS=1
            shift
        ;;
        -h|--help)
            usage
            exit 0;
        ;;
        --)
            shift
            break
        ;;
        *)
            usage
            exit 3
        ;;
    esac
done

echo ""





if [[ $INSTALL_REPOS ]]; then
    installRepos
fi

if [[ $INSTALL_PULSEAUDIO ]]; then
    installPulseAudio
fi

if [[ $INSTALL_NVIDIA ]]; then
    installNvidiaDriver
fi

if [[ $INSTALL_APPS ]]; then
    installApps
fi

if [[ $INSTALL_JDK ]]; then
    installOracleJDK
fi

if [[ $INSTALL_WPS ]]; then
    installWPS
fi

if [[ $INSTALL_VIDEO_CODECS ]]; then
    installVideoCodecs
fi