#!/bin/bash

echo "Installing php7"
zypper in -y `zypper search php7 | tail -n +6 | grep -v -i pear | grep -v -i zend | grep -v -i apache | grep -v -i devel | cut -f 2 -d "|"`

echo ""
