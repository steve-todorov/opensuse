#!/bin/bash

set -e

if [[ `zypper search -si libpng12 | tail -n +6 | wc -l` -eq 0 ]]; then
    echo "Installing libpng12 (required dependency)"
    (set -x; zypper in -y libpng12)
fi

echo "Installing WPS Office Suite..."
(set -x; rpm --force --nosignature -i --nodeps http://kdl1.cache.wps.com/ksodl/download/linux/a21//wps-office-10.1.0.5707-1.a21.x86_64.rpm)
echo ""

echo "Installing WPS fonts..."
(set -x; rpm --force --nosignature -i --nodeps http://kdl.cc.ksosoft.com/wps-community/download/fonts/wps-office-fonts-1.0-1.noarch.rpm)
echo ""

echo "Installing Microsoft fonts..."
(set -x; zypper in -y fetchmsttfonts)
echo ""
