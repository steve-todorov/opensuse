#!/bin/bash

echo "Installing PulseAudio..."
zypper in -y pulseaudio `zypper search pulseaudio-* | tail -n +6 | cut -f 2 -d "|" | grep -v "gdm" | uniq | xargs`
echo ""