#!/bin/bash

. ../setup.sh

echo "Installing video codecs..."
echo ""

echo "Checking repositories... (adding packman and dvd if necessary)"
addRepo "http://packman.inode.at/suse/openSUSE_Tumbleweed/" "pacman"
addRepo "http://opensuse-guide.org/repo/openSUSE_Tumbleweed/" "dvd"

# Checkout http://opensuse-guide.org/codecs.php
echo "Installing necessary packages."
zypper in -y ffmpeg lame gstreamer-plugins-bad gstreamer-plugins-ugly gstreamer-plugins-ugly-orig-addon gstreamer-plugins-libav libdvdcss2
echo ""