#!/bin/bash

echo "Installing Deepin"
zypper in --from deepin startdde deepin-control-center deepin-dock deepin-launcher deepin-session-ui deepin-file-manager deepin-gettext-tools
zypper in --from deepin deepin-calendar deepin-calculator deepin-music deepin-terminal deepin-screen-recorder deepin-movie deepin-system-monitor deepin-screenshot

echo ""
