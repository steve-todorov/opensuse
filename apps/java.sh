#!/bin/bash

JDK_URL_VERSION="8u131"
JDK_URL_VERSION_PREFIX="-b11"
JDK_PATH_VERSION="1.8.0_131"
JDK_HASH=d54c1d3a095b4ff2b6607d096fa80163
JDK_URL="${JDK_URL_VERSION}${JDK_URL_VERSION_PREFIX}/${JDK_HASH}/jdk-${JDK_URL_VERSION}-linux-x64"

MVN_VERSION=3.3.9

USER_ID=1000
GROUP_ID=1000

set -e

echo "Installing JDK $JDK_PATH_VERSION"

(set -x; mkdir -p /java)

echo "Downloading http://download.oracle.com/otn-pub/java/jdk/${JDK_URL}.tar.gz"

(set -x; curl -j -L -C - -b "oraclelicense=accept-securebackup-cookie" "http://download.oracle.com/otn-pub/java/jdk/${JDK_URL}.tar.gz" | tar -xz -C /java)
(set -x; chown -R ${USER_ID}.${GROUP_ID} /java/jdk*)

JAVA_HOME=/java/jdk${JDK_PATH_VERSION}

echo "Fixing JAVA_HOME env variable"
(set -x; ln -s $JAVA_HOME /usr/java)

echo "Running update-alternatives..."
(set -x; /usr/sbin/update-alternatives --install   "/usr/bin/java" "java" "${JAVA_HOME}/bin/java" 99999)
(set -x; /usr/sbin/update-alternatives --set java  "${JAVA_HOME}/bin/java")
(set -x; /usr/sbin/update-alternatives --install   "/usr/bin/javac" "javac" "${JAVA_HOME}/bin/javac" 99999)
(set -x; /usr/sbin/update-alternatives --set javac "${JAVA_HOME}/bin/javac")


echo "Installing Maven $MVN_VERSION"
(set -x; curl -L -C - "http://apache.cbox.biz/maven/maven-3/${MVN_VERSION}/binaries/apache-maven-${MVN_VERSION}-bin.tar.gz" | tar -xz -C /java)
(set -x; chown -R ${USER_ID}.${GROUP_ID} /java/apache*)

M2_HOME=/java/apache-maven-${MVN_VERSION}

echo "Fixing M2_HOME env variable"
(set -x; echo "export M2_HOME=$M2_HOME" > /etc/profile.d/maven.sh)
(set -x; chmod +x /etc/profile.d/maven.sh)

echo "Running update-alternatives..."
(set -x; /usr/sbin/update-alternatives --install   "/usr/bin/mvn" "mvn" "${M2_HOME}/bin/mvn" 99999)
(set -x; /usr/sbin/update-alternatives --set mvn   "${M2_HOME}/bin/mvn")

