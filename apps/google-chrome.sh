#!/bin/bash

if [[ `zypper search -si lsb  | grep -i "no matching" | wc -l` == 1 ]]; then
    echo "Installing lsb-release (if missing)"
    zypper in -y lsb lsb-release
fi

echo "Installing google chrome..."
rpm --force --nosignature --nodeps -i https://dl.google.com/linux/direct/google-chrome-stable_current_x86_64.rpm

